# ICS3UProjectShowcase

This website contains the files for the ICS3U website. It showcases all my projects throught ICS3U. 

The website is hosted by Spring Boot and Maven on Heroku/Docker as well as on Netlify. There is a postgreSQL server add-on hosted on Heroku with HikaruCP. 

Visit the website at https://ics3uprojectshowcase.netlify.app/!
